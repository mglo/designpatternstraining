package com.mglo.patterns.creational.factory.customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Pizza {
    String name;
    int price;
    String ciasto;
    String sos;
    List dodatki = new ArrayList();

    public abstract void przygotowanie();

    //metody ktore wykonuje pojo - strzelanie, poruszanie sie
    public void pieczenie() {
        System.out.println("Pieczenie: 25 minut w temp. 180 stopni celcjusza");
    }

    public void krojeie(){
        System.out.println("Krojenie");
    }



    public void pakowanie(){
        System.out.println("Pakowanie pizzy w oficjalne pudełko");
    }

    public void ustawNazwa(String name){
        this.name = name;
    }

    public String pobierzNazwe(){
        return name;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", ciasto='" + ciasto + '\'' +
                ", sos='" + sos + '\'' +
                ", dodatki=" + dodatki +
                '}';
    }
}
