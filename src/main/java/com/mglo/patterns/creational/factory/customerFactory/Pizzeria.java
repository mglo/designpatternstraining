package com.mglo.patterns.creational.factory.customerFactory;

import com.mglo.patterns.creational.factory.customer.Pizza;

public abstract class Pizzeria {

    public Pizza zamowPizza(String type){
        Pizza pizza;

        pizza = utworzPizza(type);

        pizza.przygotowanie();
        pizza.pieczenie();
        pizza.krojeie();
        pizza.pakowanie();

        return pizza;
    }

    protected abstract Pizza utworzPizza(String type);

    //tutaj inne metody
}
