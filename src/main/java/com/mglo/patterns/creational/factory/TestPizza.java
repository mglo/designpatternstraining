package com.mglo.patterns.creational.factory;

import com.mglo.patterns.creational.factory.customer.Pizza;
import com.mglo.patterns.creational.factory.customer.WloskaPepperoniPizza;
import com.mglo.patterns.creational.factory.customerFactory.Pizzeria;
import com.mglo.patterns.creational.factory.customerFactory.WloskaPizzeria;

public class TestPizza {

    public static void main(String[] args) {
        Pizzeria wloska = new WloskaPizzeria();

        Pizza pizza = wloska.zamowPizza("serowa");
        System.out.println("zamowiona pizza to " + pizza.pobierzNazwe());
        System.out.println(pizza.toString());

    }
}
