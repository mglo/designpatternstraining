package com.mglo.patterns.creational.factory.customer;

public class WloskaPepperoniPizza extends Pizza {

    public WloskaPepperoniPizza() {
        name = "Wloska pizza peperoni";
        sos = "Sos pomidorowy";
        ciasto = "Puszyste ciasto";
        dodatki.add("plasterek szynki");
    }

    public void przygotowanie() {
        System.out.println("Jakies przygotowanie pizzy " + name);
    }
}
