package com.mglo.patterns.creational.factory.customer;

public class WloskaSerowaPizza extends Pizza {

    public WloskaSerowaPizza() {
        super.name = "Wloska pizza serowa z  ";
        ciasto = "Cienkie kruche ciasto";
        sos = "Sos Marinara";
        dodatki.add("Tarty ser Reggiano");
    }

    public void przygotowanie() {
        System.out.println("przygotowanie: " + name);

    }


}
