package com.mglo.patterns.creational.factory.customerFactory;

import com.mglo.patterns.creational.factory.customer.Pizza;
import com.mglo.patterns.creational.factory.customer.WloskaPepperoniPizza;
import com.mglo.patterns.creational.factory.customer.WloskaSerowaPizza;

public class WloskaPizzeria extends Pizzeria {

    protected Pizza utworzPizza(String type) {
        if(type.equals("serowa")){
            return new WloskaSerowaPizza();
        }else if(type.equals("peperoni")){
            return new WloskaPepperoniPizza();
        }else return null;


    }
}
